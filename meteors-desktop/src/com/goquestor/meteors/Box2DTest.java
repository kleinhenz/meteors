package com.goquestor.meteors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class Box2DTest {
	public static void main(String[] args) {
		
		Vector2 gravity = new Vector2(0, 0);
		World world = new World(gravity, false);
		
		
		BodyDef bodyDef = new BodyDef();
		Body body = world.createBody(bodyDef);
		
		for (int i = 0; i < 60; i++) {
		    world.step(1f / 60f, 6, 2);
		    System.out.println(body.getPosition().y);
		}
		
	}
}

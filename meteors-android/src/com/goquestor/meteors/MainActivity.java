package com.goquestor.meteors;

import tv.ouya.console.api.OuyaController;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class MainActivity extends AndroidApplication {

	private MeteorGame game = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = true;
        OuyaController.init(this);
        
        game = new MeteorGame();
        initialize(game, cfg);
    }
    
    @Override
    public boolean onKeyDown(final int keyCode, KeyEvent event){
        //Get the player #
        int player = OuyaController.getPlayerNumByDeviceId(event.getDeviceId());       
        boolean handled = false;

        //Handle the input
        switch(keyCode){
            case OuyaController.BUTTON_O:
                //You now have the key pressed and the player # that pressed it
                //doSomethingWithKey();
                handled = true;
                break;
        }
        return handled || super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onGenericMotionEvent(final MotionEvent event) {
        //Get the player #
        int player = OuyaController.getPlayerNumByDeviceId(event.getDeviceId());    

        //Get all the axis for the event
        float LS_X = event.getAxisValue(OuyaController.AXIS_LS_X);
        float LS_Y = event.getAxisValue(OuyaController.AXIS_LS_Y);
        float RS_X = event.getAxisValue(OuyaController.AXIS_RS_X);
        float RS_Y = event.getAxisValue(OuyaController.AXIS_RS_Y);
        float L2 = event.getAxisValue(OuyaController.AXIS_L2);
        float R2 = event.getAxisValue(OuyaController.AXIS_R2);
        
        //Do something with the input
        game.updatePlayerInput(player, LS_X, LS_Y, RS_X, RS_Y, L2, R2);
        /*
        if (LS_X>=OuyaController.STICK_DEADZONE &&
				LS_X>=OuyaController.STICK_DEADZONE &&
				LS_Y>=OuyaController.STICK_DEADZONE &&
				LS_Y>=OuyaController.STICK_DEADZONE) {
		}
		*/

        return true;
    }
}
package com.goquestor.meteors;

import com.badlogic.gdx.Game;

public class MeteorGame extends Game {
	
	private LevelScreen levelScreen = null;
	
	@Override
	public void create() {		
		// create view for level
		levelScreen = new LevelScreen();
		setScreen(levelScreen);
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public void updatePlayerInput(int player, float lS_X, float lS_Y,
			float rS_X, float rS_Y, float l2, float r2) {
		if (levelScreen!=null)
			levelScreen.updatePlayerInput(player, lS_X, lS_Y, rS_X, rS_Y, l2, r2);
	}
}
